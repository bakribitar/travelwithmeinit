var express = require('express');
var env = process.env.NODE_ENV =  process.env.NODE_ENV || 'production';
var path = require ('path');

if(env== 'development')
{
    var connectionString = "mongodb://localhost:27017/TravelWithMe";
}
else
{
    var connectionString = "mongodb://admin:admin@ds053126.mlab.com:53126/travelwithme";
}

stylus = require ('stylus');

logger = require('morgan');
bodyParser = require('body-parser');
mongoose = require('mongoose');
var app = express();

function compile   (str,path)
{
    return stylus(str).set('filename',path);
}

app.set('views',path.join(__dirname ,'server','views'));

app.set('view engine', 'jade');
app.use(logger('dev'));

/* just for the last version of Angular:*/
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.use(stylus.middleware(
    {
        src: path.join(__dirname ,'public'),
        compile: compile
    }
));


app.use(express.static(path.join(__dirname ,'public')));

mongoose.connect(connectionString);
var db = mongoose.connection;
db.on('error',console.error.bind(console,'connection error ...'));
db.once('open',function callback() {
    console.log('TWM Db opened')
});



var MongoMessage;

app.get('/partials/:partialPath', function(req, res) {
    res.render('partials/' + req.params.partialPath);
});

/* create schema for messages for example*/
var schema = mongoose.Schema({message : String});
/* create Model*/
var message = mongoose.model('message',schema);

app.get('/messages',function (req,resp){
    message.find(function(err,messages) {
        resp.send(messages);
    })

});

var usersSchema = mongoose.Schema({name: String},{BDate: String},{Gender: String});
var usersModel = mongoose.model('users',usersSchema);
app.get('/users', function (req,resp) {
    usersModel.find(function (err,users) {

        resp.render('index', {
            MongoMessage: users
        });
            // resp.send(users);
    })
})

app.get('/signup',function (req,resp) {
    resp.render('index');
})
// just handle every request by the index
app.get('*',function (req,resp){


    resp.render('landingPage');
        // ,
        // {
        //     MongoMessage:"Main Page"
        // });
});

var port = process.env.PORT || 3030;
app.listen(port);