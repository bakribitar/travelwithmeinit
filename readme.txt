To run this application, start your mongo server & do the following from the command line: 
P.S. please make sure that your working directory in the command line is the same as your local repo.

bower install
npm install
nodemon server.js OR just node server.js and then you can open http://localhost:3030/ in your browser.
----------------------------------------------------
MongoDB:
We use a Database-as-a-Service. Connection strings for it are as follows:
	To connect using the mongo shell:	mongo ds053126.mlab.com:53126/travelwithme -u admin -p admin
	To connect using a driver via the standard MongoDB UR: mongodb://admin:admin@ds053126.mlab.com:53126/travelwithme
Right now the DB contains just test Data, feel free to give it a basic structure :)
----------------------------------------------------
Jade Files

Jade is a language used to create HTML templates on the server. This allows us to write HTML much easier because it avoid having to write as many brackets and also allows for us to bind a view to the underlying data model (similar to what Angular.js does in the browser).

Stylus Files

Stylus is the Jade for CSS. It allows us to write CSS much easier than the raw CSS language.

N.B: Please refer to Jade (you might see it on the Internet with the name PUG ) and Stylus while creating the UI.
We can use something else if you want to, but for now I have set the preferences to use Jade and Stylus, in case we need to use something else, we need then to change the references. 

Edit: jade and stylus can render html and css raw files, so if you prefer to work with raw html and css you don't have to worry about jade and stylus. Just do as in the demo page "landingPage".
----------------------------------------------------------